<?php
require_once 'index.php';

class AddTest extends PHPUnit\Framework\TestCase {
    public function testAdd() {
        $this->assertEquals(3, add(1,2));
    }
}